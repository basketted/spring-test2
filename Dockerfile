FROM openjdk:11-alpine
ENTRYPOINT ["/usr/bin/spring-test2.sh"]

COPY spring-test2.sh /usr/bin/spring-test2.sh
COPY target/spring-test2.jar /usr/share/spring-test2/spring-test2.jar
